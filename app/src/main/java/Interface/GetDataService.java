package Interface;

import java.util.List;

import Model.ResponsePhoto;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {
    @GET("/photos")
    Call<List<ResponsePhoto>> getAllPhotos();
}
