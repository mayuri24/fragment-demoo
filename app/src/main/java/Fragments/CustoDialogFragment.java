package Fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.fragmentdemo.R;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

public class CustoDialogFragment extends Fragment {
    private LinearLayoutManager mLayoutManager;
    Context mContext;
    Button mBtnbtnCustomDialogue;
    Button mBtnOk;
    Button mBtnCancel;

    public CustoDialogFragment() {
        /* compiled code */
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater,
                                          android.view.ViewGroup container, android.os.Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_customdialog, container, false);
        bindViews(rootView,container);
        return rootView;
    }

    private void bindViews(View rootView, ViewGroup container) {

        mContext = container.getContext();
        mBtnbtnCustomDialogue = (Button) rootView.findViewById(R.id.btnCustomDialogue);
        mBtnbtnCustomDialogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCustomDialogue();
            }
        });
    }

    private void openCustomDialogue() {
        LayoutInflater vi;
        vi = LayoutInflater.from(mContext);
        View dialogueLayout = vi.inflate(R.layout.custom_dialogue_view, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogueLayout);
        builder.setCancelable(false);
        builder.setMessage("CUSTOM ALERT");

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        mBtnOk = (Button) dialogueLayout.findViewById(R.id.buttonOk);
        mBtnCancel = (Button) dialogueLayout.findViewById(R.id.buttonCancel);

        //SETUP LISTENERS
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "You Pressed Cancel", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
