package Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.fragmentdemo.R;

import java.util.List;

import Adapter.CustomAdapter;
import Interface.GetDataService;
import Model.ResponsePhoto;
import Network.RetrofitClientInstance;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerFragment extends Fragment {
    Context mContext;
    private CustomAdapter adapter;
    ProgressDialog progressDialog;
    private androidx.recyclerview.widget.RecyclerView mRecycler_item_list;

    public RecyclerFragment() {
        /* compiled code */
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater,
                                          android.view.ViewGroup container, android.os.Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.recycler_view_activity, container, false);
        bindViews(rootView, container);
        fetchApi();
        return rootView;
    }

    private void bindViews(View rootView, ViewGroup container) {

        mContext = container.getContext();
        mRecycler_item_list = (androidx.recyclerview.widget.RecyclerView) rootView.findViewById(R.id.recycler_item_list);
    }

    private void fetchApi() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<ResponsePhoto>> call = service.getAllPhotos();

        call.enqueue(new Callback<List<ResponsePhoto>>() {
            @Override
            public void onResponse(Call<List<ResponsePhoto>> call, Response<List<ResponsePhoto>> response) {
                progressDialog.dismiss();
                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<ResponsePhoto>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(mContext, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void generateDataList(List<ResponsePhoto> photoList) {
        adapter = new CustomAdapter(mContext, photoList) {
            @Override
            public void onItemClick(int Position, View view, ResponsePhoto responsePhoto) {

                Toast.makeText(mContext, responsePhoto.getTitle(), Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(mContext, RecyclerDataActivity.class);
//                i.putExtra("message", responsePhoto.getTitle());
//                startActivity(i);


            }
        };
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecycler_item_list.setLayoutManager(layoutManager);
        mRecycler_item_list.setAdapter(adapter);

    }


}
