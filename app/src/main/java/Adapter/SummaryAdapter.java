package Adapter;


import Fragments.CustoDialogFragment;
import Fragments.RecyclerFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class SummaryAdapter extends FragmentStatePagerAdapter {

    Integer numberOfTabs;// This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created


    public SummaryAdapter(FragmentManager fm, int tabs) {
        super(fm);

        this.numberOfTabs = tabs;

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)      // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            DebitReportFragment surchargeReportFragment = new DebitReportFragment();
            return surchargeReportFragment;
        }
        if (position == 1)      // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            CustoDialogFragment custoDialogFragment = new
                    CustoDialogFragment();
            return custoDialogFragment;

        }
        if (position == 2)      // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            RecyclerFragment recyclerFragment = new RecyclerFragment();
            return recyclerFragment;
        }

      else
            return null;

    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}