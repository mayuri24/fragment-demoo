package Adapter;

import android.view.View;

import com.example.fragmentdemo.R;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

public class DebitReportFragment extends Fragment {
    private LinearLayoutManager mLayoutManager;
    android.content.Context mContext;
    public DebitReportFragment() { /* compiled code */ }

    public android.view.View onCreateView(android.view.LayoutInflater inflater,
                                          android.view.ViewGroup container, android.os.Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_debit, container, false);
        bindViews(rootView);
        return rootView;
    }

    private void bindViews(View rootView) {

    }
}
