package com.example.fragmentdemo;

import Adapter.SummaryAdapter;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
    private androidx.viewpager.widget.ViewPager mViewpager;
    SummaryAdapter adapter;
    Context mContext;
    RelativeLayout relativeLayout;
    com.google.android.material.tabs.TabLayout mDetail_tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
    }
    private void bindViews() {
        mContext = MainActivity.this;
        relativeLayout = (RelativeLayout) findViewById(R.id.main_content);

        mDetail_tabs = (com.google.android.material.tabs.TabLayout) findViewById(R.id.detail_tabs);
        mViewpager = (androidx.viewpager.widget.ViewPager) findViewById(R.id.viewpager);

        mDetail_tabs.addTab(mDetail_tabs.newTab().setText("Login"));
        mDetail_tabs.addTab(mDetail_tabs.newTab().setText("CustomDialog eg"));
        mDetail_tabs.addTab(mDetail_tabs.newTab().setText("RecyclerList"));

        mDetail_tabs.setTabMode(TabLayout.MODE_FIXED);

        adapter = new SummaryAdapter(getSupportFragmentManager(), mDetail_tabs.getTabCount());
        mViewpager.setAdapter(adapter);


        mViewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mDetail_tabs));

        mDetail_tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Toast.makeText(mContext, "unselected Tab", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Toast.makeText(mContext, "Re-selected Tab", Toast.LENGTH_SHORT).show();

            }
        });


    }
}
